#include <iostream>
#include <string>
#include "AVL.hpp"
#include <vector>
#include <map>
#include <list>
#include "heap.hpp"

using namespace algorithms;
using namespace std;

class App;
typedef void (App::* func)(void);

class App{

private:


    // could be expanded with min_element, max_element, contains, successor, predecessor
    // needed to register the operations in an expandable way
    const map<const string, pair<const string, func>> operations{
        {"insert", {"Insert one element (integers) into the tree.", &App::insert }},
        {"delete", {"Delete one element from the tree (if exists).", &App::del}},
        {"exit", {"Exit the program.", &App::ex }}
    };

    AVLTree<int> tree;
    bool cflag = true;

    void insert(){
        int x;
        cin >> x;
        if (!cin.fail()){

            tree.insert(x,x);
            cout << static_cast<string>(tree) << endl;
        }else{
            string tmp; cin.clear(); getline(cin, tmp, '\n'); cout << "Incorrect input: " << tmp << endl;
        }
    };
    void del(){
        int x;
        cin >> x;
        if (!cin.fail()){
            tree.remove(x);
            cout << static_cast<string>(tree) << endl;
        }else{
            string tmp; cin.clear(); getline(cin, tmp, '\n'); cout << "Incorrect input: " << tmp << endl;
        }
    };
    void ex(){cflag = false;};

    string read_operation(){
        string inst;
        cin >> inst;
        return inst;
    };

    void help(){
        cout << string(120, '-') << endl;
        cout << "How to use.\nType one of the instructions listed below into the console and hit return.\n"
                "Than type the necessary parameter and hit return. The current state of the tree is printed on the screen.\n"
        "e.g.:\n> insert\n> 15\n> ( ( 10 )<-15->( 30 ) )\n\n";

        for (auto operation : operations){
            cout << "\'" << operation.first <<"\' -> "  << operation.second.first << "\n";
        }

        cout << string(120, '-') << endl;
    };

    void event_loop(){
        while(cflag){
            string instr = read_operation();

            if(1 == operations.count(instr)){
                func f = operations.find(instr)->second.second; // returns member function
                (this->*f)(); // calls member function
            }
            else{
                cout << "\n\'" << instr << "\' - "  << "The typed instruction is invalid!\n";
                help();
            }
        };
    }

public:
    void run (void){
        help();
        event_loop();

    };



};



int main() {

    App application = App();

    application.run();
    return 0;
}
