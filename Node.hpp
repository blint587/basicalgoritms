
#ifndef BASICALGORITMS_NODE_H
#define BASICALGORITMS_NODE_H

#include <memory>
namespace algorithms {
    template<class T, class B>
    class Node {

    public:
        const int key;
        const T data;

        Node(int key, T data) : key(key), data(data), left(nullptr), right(nullptr) {}

        std::shared_ptr<B> left;
        std::shared_ptr<B> right;

    };

}

#endif //BASICALGORITMS_NODE_H
