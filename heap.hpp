#include <vector>
#include <iostream>


#ifndef BASICALGORITMS_HEAP_HPP
#define BASICALGORITMS_HEAP_HPP

template<class T>
void lower(std::vector<T> & h, long long i, long long m, T x){
    long long j = 2 * i +1;
    bool b = true;
    while(j <= m && b){

        if(j < m && h[j+1] > h[j]){
            j = j+1;
        }
        if(h[j] > x){
            h[i] = h[j];
            i = j; j = 2*j;
        }
        else{
            b = false;
        }
    }
    h[i] = x;

}


template<class T>
void to_heap( std::vector<T> & h){
    for(long long k = (h.size()/ 2); k >= 0; k--){
        std::cout << "from: "<< k << std::endl;
        lower(h, k, h.size(), h[k]);
    }
}



#endif //BASICALGORITMS_HEAP_HPP
