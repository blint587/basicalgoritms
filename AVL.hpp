
#ifndef BASICALGORITMS_AVL_HPP
#define BASICALGORITMS_AVL_HPP

#include <memory>
#include <sstream>
#include <string>
#include <iostream>
#include "Node.hpp"


namespace algorithms {

    template <class T>
    class AVLNode : public Node<T, AVLNode<T>> {
        template<class U> friend class AVLTree;

    public:
        // implicit string cast operator
        operator std::string() const{
            std::stringstream st;
            std::string left = (this->left == nullptr) ? "": static_cast<std::string>(*(this->left)) + "<-";
            std::string right = (this->right == nullptr) ? "": "->" + static_cast<std::string>(*(this->right));

            st << "( " << left << this->key <<  right  <<  " )";
            return st.str();
        };

    private:
        int height = 1;
    public:
        AVLNode(int key, T data) : algorithms::Node<T, AVLNode>(key, data) {}
        bool is_leaf() const{
            return (this->left == nullptr && this->right == nullptr);
        }

    };


    template<class T>
    class AVLTree {

    public:
        AVLTree(){};
        AVLTree(int key, T data): root(std::make_shared<AVLNode<T>>(key, data)){
        };

        //
        operator std::string() const{
            if (root) {
                return static_cast<std::string>(*root);
            }else{
                return "()";
            }
        }
        void insert(int key, T data){insert(key, data, root);};
        void remove(int key){remove(root, key);};

    private:

        std::shared_ptr<AVLNode<T>> root;

        int get_height(const std::shared_ptr<AVLNode<T>> &n) const {
            return (n == nullptr ?  -1 : n->height);
        }

        int balance_factor(const std::shared_ptr<AVLNode<T>> &n) const {
            return get_height(n->right) - get_height(n->left);
        }

        void update_height(const std::shared_ptr<AVLNode<T>> &n) {
            int hlelft = get_height(n->left);
            int hright = get_height(n->right);
            n->height = (hlelft > hright ? hlelft : hright) + 1;
        }

        void rotate_right(std::shared_ptr<AVLNode<T>> &p) {
            /*
                        p ___                               q ___
                         |_y_|                               |_x_|
                        /     \                             /     \
                    q__/       \                           /        p___
                    |_x_|       \          ==>            /         |_y_|
                   /     \       \                       /         /     \
                  /\     /\       \                     /         /\     /\
                 /  \   /  \      /\                   /\        /  \   /  \
                /_AA_\ /_BB_\    /  \                 /  \      /_BB_\ /_CC_\
                                /_CC_\               /_AA_\
            */
            std::shared_ptr<AVLNode<T>> q(p->left);
            p->left = q->right;
            q->right = p;
            update_height(p);
            update_height(q);
            p = q;
        }

        void rotate_left(std::shared_ptr<AVLNode<T>> &q) {

         /*
                     q ___                                       p ___
                      |_x_|                                       |_y_|
                     /     \                                     /     \
                    /        p___                            q__/       \
                   /         |_y_|         ==>              |_x_|        \
                  /         /     \                        /     \        \
                 /         /\     /\                      /\     /\        \
                /\        /  \   /  \                    /  \   /  \       /\
               /  \      /_BB_\ /_CC_\                  /_AA_\ /_BB_\     /  \
              /_AA_\                                                     /_CC_\
        */

            std::shared_ptr<AVLNode<T>> p(q->right);
            q->right = p->left;
            p->left = q;
            update_height(q);
            update_height(p);
            q = p;
        }

        void balance(std::shared_ptr<AVLNode<T>> &p){
            update_height(p);
            int bfactor = balance_factor(p);
            if(2 == bfactor){
                if(balance_factor(p->right) < 0 )
                    rotate_right(p->right);
                rotate_left(p);
            }
            else if (-2 == bfactor)
            {
                if(balance_factor(p->left) > 0  )
                    rotate_left(p->left);
                rotate_right(p);
            }

        }

         void insert(int key, T data, std::shared_ptr<AVLNode<T>> & r){
            if( !r ) {
                r = std::make_shared<AVLNode<T>>(key, data);
            }
            else if( key <  r->key) {
                insert(key, data, r->left);
            }
            else if(key > r->key) {
               insert(key, data, r->right);
            }
            balance(r);
        }

        // find a node with minimal key in a p tree
        std::shared_ptr<AVLNode<T>> find_min(std::shared_ptr<AVLNode<T>> &p){
            return p->left != nullptr ? find_min(p->left):p;
        }

        // deleting a node with minimal key from a p tree
        std::shared_ptr<AVLNode<T>> remove_min(std::shared_ptr<AVLNode<T>> &p)
        {
            if( p->left== nullptr ) {
                return p->right;
            }else {
                p->left = remove_min(p->left);
            }
            balance(p);
            return p;
        }

        // deleting k key from p tree
        void remove(std::shared_ptr<AVLNode<T>> & p, int k)
        {
            if( p == nullptr ){
                return;
            }
            else if( k < p->key ) {
                remove(p->left, k);
            }
            else if( k > p->key ) {
                remove(p->right, k);
            }
            else if (k == p->key){
                std::shared_ptr<AVLNode<T>> q = p->left;
                std::shared_ptr<AVLNode<T>> r = p->right;
                p.reset(); //deletes the owned object
                if( r == nullptr ){
                    p = q;
                    return;
                }
                else{
                    std::shared_ptr<AVLNode<T>> min = find_min(r);
                    min->right = remove_min(r);
                    min->left = q;
                    balance(min);
                    p = min;
                    return;
                }


            }
            balance(p);
        }
    };

}
#endif //BASICALGORITMS_AVL_HPP
